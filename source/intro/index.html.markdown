---
layout: "intro"
page_title: "Introduction"
sidebar_current: "what"
description: |-
  Welcome to the intro guide to Terraform! This guide is the best place to start with Terraform. We cover what Terraform is, what problems it can solve, how it compares to existing software, and contains a quick start for using Terraform.
---

# Introduction to Terraform

Welcome to the intro guide to Terraform! This guide is the best
place to start with Terraform.
ここでは，Terraformとは何か，どのような課題を解決できるか，既存
のソフトウェアと比較するとどうか，Terraformを用いたクイックスター
トについて取り扱います．

もしTerraformの基礎について把握しているのであれば，
[ドキュメンテーション](/docs/index.html) を確認することで
すべての機能や内部について参照することができます．

## What is Terraform?

Terraformはインフラを安全かつ効率的にビルド，変更，バージョニングするため
のツールです．Terraformは既存の主要なサービスプロバイダやカスタマイズした
自製の手法を扱うことができます．

Configuration files describe to Terraform the components needed to
run a single application or your entire datacenter.
Terraform generates an execution plan describing
what it will do to reach the desired state, and then executes it to build the
described infrastructure. As the configuration changes, Terraform is able
to determine what changed and create incremental execution plans which
can be applied.

Terraformが扱うことができるインフラには低レベルの要素であるコンピュータ
インスタンス，ストレージ，ネットワークや高レベルの要素であるDNSエントリ，
SaaS機能などがあります．

例を見ることでTerraformを把握することができます．
[ユースケース](/intro/use-cases.html) を確認して下さい.

Terraformの主要機能として以下があります：

* **Infrastructure as Code**: Infrastructure is described using a high-level
  configuration syntax. This allows a blueprint of your datacenter to be
  versioned and treated as you would any other code. Additionally,
  infrastructure can be shared and re-used.

* **Execution Plans**: Terraform has a "planning" step where it generates
  an _execution plan_. The execution plan shows what Terraform will do when
  you call apply. This lets you avoid any surprises when Terraform
  manipulates infrastructure.

* **Resource Graph**: Terraform builds a graph of all your resources,
  and parallelizes the creation and modification of any non-dependent
  resources. Because of this, Terraform builds infrastructure as efficiently
  as possible, and operators get insight into dependencies in their
  infrastructure.

* **Change Automation**: Complex changesets can be applied to
  your infrastructure with minimal human interaction.
  With the previously mentioned execution
  plan and resource graph, you know exactly what Terraform will change
  and in what order, avoiding many possible human errors.

## Next Steps

[Terraform ユースケース](/intro/use-cases.html)
のページではTerraformの複数の利用方法を確認できます．
その後，
[how Terraform compares to other software](/intro/vs/index.html)
では既存のインフラに合わせることができるかを確認できます．
最後に，
[getting started guide](/intro/getting-started/install.html)
を見ることで Terraformを利用して実際のインフラを管理する
方法を確認できます．
